require('../node_modules/nightwatch/bin/runner.js');

module.exports = {
  "AutoTas" : function (browser) {
    browser
      .url("https://secure.innotas.com/index.jsp")
      .waitForElementVisible('#SSOBtn', 5000)
      .click('#SSOBtn')

      .pause(1000)
      
      .setValue('input#login', 'alan.friedman@weather.com')
      
      .click('#SSOForm button')

      .waitForElementVisible('.dashBoardTable', 5000)

      .end();
  }
};